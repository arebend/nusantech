function maze(param) {
  // Nilai n harus positif dan n dapat dinyatakan dalam 4n - 1
  if ((param + 1) % 4 !== 0 || param <= 0) console.log('Input must be a positive integer/number')
  // Menggunakan nested loop untuk dapat akses nxn
  for (let i = 0; i < param; i++) {
    // initial value
    let result = ''
    for (let j = 0; j < param; j++) {
      // jika index i dan j mempunyai nilai berikut lakukan proses didalamnya
      if (i === 0 || i === param - 1 || j === 0 || j === param - 1 || i % 2 === 0) {
        // jika index i habis dibagi 4 dan nilai j = 1 beri spasi
        if (i % 4 === 0 && j === 1) result += ' '
        //  jika index i habis dibagi 2 dan tidak habis dibagi 4 dan index j nilainya sama dengan n - 2 beri spasi
        else if (i % 2 === 0 && i % 4 !== 0 && j === param - 2) result += ' '
        //  jika tidak ada di statement diatas beri '@'
        else result += '@'
      } else {
        // jika tidak memenuh statement diatas beri spasi
        result += ' '
      }
    }
    console.log(result)
  }
}
maze('param')
maze(-1)
maze(0)
maze(15)